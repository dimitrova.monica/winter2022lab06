public class Board {
    private Die dieOne;
    private Die dieTwo;
    private boolean[] closedTiles;

    public Board(){
        this.dieOne = new Die();
        this.dieTwo = new Die();
        this.closedTiles = new boolean[12];
    }

    public boolean playATurn(){
        dieOne.roll();
        dieTwo.roll();
        boolean result = false;

        System.out.println("The first die shows: " + dieOne);
        System.out.println("The second die shows: " + dieTwo);

        int sum = dieOne.getPips() + dieTwo.getPips();

        if(closedTiles[sum-1] == false){
            closedTiles[sum-1] = true;
            System.out.println("Closing tile: " + (sum));
            System.out.println(toString());
            result = false;
        } else if (closedTiles[sum-1] == true){
            System.out.println("The tile at this position is already shut!");
            System.out.println(toString());
            result = true;
        }
        return result;
    }

    public String toString() {
        String tiles = " ";
        for(int i = 0 ; i < closedTiles.length ; i++){

            if (closedTiles[i] == false){
                tiles += (i+1 + " ");
            } else if (closedTiles[i] == true) {
                tiles += "X ";
            }
        }
        return "Shut The Box: " + tiles;
    }
}
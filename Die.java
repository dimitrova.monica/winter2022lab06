import java.util.Random;

public class Die {
    private int pips;
    private Random random;

    public Die() {
        this.pips = 1;
        this.random = new Random();
    }

    public int getPips() {
        return pips;
    }

    public Random getRandom() {
        return random;
    }

    public int roll(){
        int min = 1;
        int max = 6;
        this.pips = random.nextInt(max - min + 1) + min;
        return this.pips;
    }

    public String toString() {
        return ""+this.pips;
    }
}
